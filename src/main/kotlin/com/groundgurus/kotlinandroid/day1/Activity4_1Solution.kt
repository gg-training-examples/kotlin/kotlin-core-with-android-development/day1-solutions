package com.groundgurus.kotlinandroid.day1

class Employee(val firstName: String, val lastName: String, val department: String) {
  fun printDetails() {
    println("Employee Name: $firstName $lastName")
    println("Department: $department")
  }
}

fun main() {
  val emp1 = Employee("John", "Smith", "Management")
  emp1.printDetails()
}
